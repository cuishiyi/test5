# 实验5：包，过程，函数的用法

- 学号：202010414308  姓名：李雅婷  班级：2020级软件工程3班

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

```
create or replace PACKAGE MyPack IS
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```

![pict1](./pict01.png)

创建一个包(Package)，包名是MyPack。

1. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
2. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
  Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID， FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 测试

```sh

函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	   10 Administration			     4400
	   20 Marketing 			    19000
	   30 Purchasing			    24900
	   40 Human Resources			     6500
	   50 Shipping				   156400
	   60 IT				    28800
	   70 Public Relations			    10000
	   80 Sales				   304500
	   90 Executive 			    58000
	  100 Finance				    51608
	  110 Accounting			    20308

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	  120 Treasury
	  130 Corporate Tax
	  140 Control And Credit
	  150 Shareholder Services
	  160 Benefits
	  170 Manufacturing
	  180 Construction
	  190 Contracting
	  200 Operations
	  210 IT Support
	  220 NOC

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	  230 IT Helpdesk
	  240 Government Sales
	  250 Retail Sales
	  260 Recruiting
	  270 Payroll

```

![pict2](./pict02.png)

## 总结

本次实验的主要目的是让我们了解PL/SQL语言的基本结构与用法，通过学习变量和常量的声明和使用方法，以及包、过程和函数的创建和调用等操作，掌握PL/SQL编程的基础知识。PL/SQL是Oracle数据库中的一种过程化编程语言，其扩展了普通的SQL语言，拥有多种特性和功能。

其中，变量和常量是PL/SQL语言中重要的数据类型之一，能够为程序提供存储和处理数据的基础；循环语句、条件语句等控制结构则可以让我们实现更复杂的逻辑运算；异常处理机制则可以在程序执行中遇到错误时进行有效的处理，保证程序的鲁棒性和稳定性。

此外，包是PL/SQL语言中一个非常有用的工具，在其中可以定义数据类型、游标、子程序等内容，并且可以将其作为一个整体进行统一管理和调用，这对于长期维护和升级的系统来说尤为重要。

通过本次实验，我深入学习了PL/SQL语言的基础知识和常用用法，获得了一定的编程经验和技巧。我相信这些所学内容对我今后从事Oracle数据库开发和管理方面的工作也会有很大的帮助。
